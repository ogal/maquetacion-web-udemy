/* 
* Equivalente al Main en Java. 
*/
$(document).ready(function(){

    activeSetup();

    // Código para hacer el botón de hamburguesa pulsable y desplegar la lista.
    $('.myicon').click(function(){
        $('#navbar ul').toggleClass('small-screen');
    });
});

/**
* Esta función aplica al objeto del navegador una clase según la página en la que
* nos encontremos. 
*/
function activeSetup(){
    // Definimos una variable para tomar el nombre de la url a través del objeto window
    var urlPath = window.location.pathname;
    // console.log(urlPath);

    $('nav a').each(function(){
        // console.log($(this));

        var href = $(this).attr('href');
        // console.log(href);
        // console.log(urlPath.substring(0, href.substr));

        if(urlPath === "/pages/".concat(href)){
            // console.log('match');
            // Aplicamos la clase CSS que definimos en nuestra hoja de estilos
            $(this).closest('li').addClass('active');
        }
    });
};/* End function activeSetup */